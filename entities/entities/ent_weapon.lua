AddCSLuaFile()

ENT.Base = "base_anim"

ENT.WModel = "models/weapons/w_crowbar.mdl"
ENT.WClass = "weapon_crowbar"
ENT.WImpactSound = "Weapon_Crowbar.Melee_HitWorld"
ENT.WImpactSoundHard = "Weapon_Crowbar.Melee_HitWorld"

ENT.SwitchOnGive = true
ENT.AmmoGive = -1

ENT.DieTime = -1

local function spm(snd, ...)
	local prop = sound.GetProperties(snd)

	if not prop then return snd, ... end
	local path = prop.sound

	if istable(path) then
		path = path[math.random(1, #path)]
	end

	return path, ...
end

if SERVER then
	function ENT:Initialize()
		self:SetModel(self.WModel)
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)

		if self.DieTime > 0 then
			SafeRemoveEntityDelayed(self, self.DieTime)
		end
	end

	function ENT:OnGive(wep) end

	function ENT:Give(ply)
		local class = self.WClass

		if not ply:HasWeapon(class) then
			local wep = ply:Give(class)

			if IsValid(wep) then
				self:OnGive(wep)
				
				wep:SetColor(self:GetColor())
				
				if self.SwitchOnGive then
					ply:SelectWeapon(class)
				end
			end
		else
			local wep = ply:GetWeapon(class)
			local ammo = wep:GetPrimaryAmmoType()
			local amnt = self.AmmoGive

			if amnt == -1 then
				amnt = wep:GetMaxClip1()
			end

			ply:GiveAmmo(amnt, ammo)
		end

		self:Remove()
	end

	function ENT:Use(ply)
		if IsValid(ply) and ply:IsPlayer() then
			self:Give(ply)
		end
	end

	function ENT:PhysicsCollide(data)
		if data.Speed <= 20 then return end

		local vol = math.Clamp(data.Speed, 0, 5000) / 5000

		self:EmitSound(spm(data.Speed >= 600 and self.WImpactSoundHard or self.WImpactSound, 85, 100, vol))
	end

	local meta = debug.getregistry().Player
	
	function meta:Give(class)
		local e = ents.Create(class)
		if not IsValid(e) then return NULL end
			e:SetPos(self:GetPos())
			e.Given = true
			e:SetNoDraw(true)
			e:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
			e.Owner = self
			e:SetOwner(self)
		e:Spawn()
		e:Activate()
		
		timer.Simple(0.1, function() if IsValid(e) then e.Given = nil end end)
		
		return e
	end
	
	function meta:ThrowWeapon()
		local wep = self:GetActiveWeapon()

		if wep:IsValid() and wep:IsWeapon() then
			local wm = wep:GetWeaponWorldModel()
			local class = wep:GetClass()
			local crow = wep.IsCrowbar or wep.Crowbar

			self:EmitSound("Zombie.AttackMiss")

			local ent = ents.Create("ent_weapon")

			ent:SetPos(self:GetShootPos() + self:GetAimVector() * 10)
			ent:SetAngles(self:GetAngles())
			ent.WModel = wm
			ent.WClass = class
			
			ent:SetColor(wep:GetColor())
			
			wep:Remove()
			ent:Spawn()
			ent:Activate()
			
			local gpo = ent:GetPhysicsObject()
			if IsValid(gpo) then
				local vel = math.min(350 + self:GetAbsVelocity():Length() / 3, 500)

				gpo:AddVelocity(self:GetAimVector() * vel)
				gpo:AddAngleVelocity(VectorRand() * vel)
			end
		end
	end
	
end