AddCSLuaFile()

SWEP.Base = "weapon_twitch_base"

SWEP.HoldType			= "smg"
SWEP.PrintName				= "AWP"

SWEP.Slot					= 4
SWEP.IconLetter				= "r"

SWEP.ViewModel				= "models/weapons/v_snip_awp.mdl"
SWEP.WorldModel				= "models/weapons/w_snip_awp.mdl"
SWEP.ViewModelAimPos		= Vector(4.9601, -0.8142, 0.206)
SWEP.ViewModelAimAng		= Vector(2.9663, 1.616, 3.3092)
SWEP.ViewModelFlip			= true

SWEP.DrawAmmo				= false
SWEP.DrawCrosshair			= false

SWEP.Primary.Sound			= Sound("Weapon_AWP.Single")
SWEP.Primary.Damage			= 99
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.002
SWEP.Primary.ConeZoomed		= 0.0005
SWEP.Primary.Delay			= 1.5

SWEP.Primary.ClipSize		= 5
SWEP.Primary.DefaultClip	= 20
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "SniperRound"

SWEP.Recoil					= 6.5

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

if CLIENT then killicon.AddFont("weapon_awp", "CSKillIcons", "r", Color(255, 220, 0, 255)) end
