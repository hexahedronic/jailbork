AddCSLuaFile()
	
SWEP.HoldType			= "pistol"

SWEP.Base = "weapon_twitch_base"

SWEP.Spawnable				= true
SWEP.AdminSpawnable			= true

SWEP.PrintName				= "Fiveseven"

SWEP.Slot					= 1
SWEP.IconLetter				= "u"

SWEP.ViewModel				= "models/weapons/v_pist_fiveseven.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_fiveseven.mdl"
SWEP.ViewModelAimPos		= Vector(3.8657, -1.1468, 2.683)
SWEP.ViewModelFlip			= true

SWEP.DrawAmmo				= false
SWEP.DrawCrosshair			= false

SWEP.Primary.Sound			= Sound("Weapon_Fiveseven.Single")
SWEP.Primary.BullettimeSound		= Sound("weapons/fiveseven/fiveseven-1.wav")
SWEP.Primary.BullettimeSoundPitch	= 70
SWEP.Primary.Damage			= 14
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.021
SWEP.Primary.ConeZoomed		= 0.008
SWEP.Primary.Delay			= 0.11

SWEP.Primary.ClipSize		= 21
SWEP.Primary.DefaultClip	= 180
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Recoil					= 0.88

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

if CLIENT then killicon.AddFont("weapon_fiveseven", "CSKillIcons", "u", Color(255, 220, 0, 255)) end
