AddCSLuaFile()
	
SWEP.HoldType			= "pistol"

SWEP.Base = "weapon_twitch_base"

SWEP.PrintName				= "Glock"

SWEP.Slot					= 1
SWEP.IconLetter				= "c"

SWEP.ViewModel				= "models/weapons/v_pist_glock18.mdl"
SWEP.WorldModel				= "models/weapons/w_pist_glock18.mdl"
SWEP.ViewModelAimPos		= Vector(3.5539, -0.9018, 2.3371)
SWEP.ViewModelFlip			= true

SWEP.DrawAmmo				= false
SWEP.DrawCrosshair			= false

SWEP.Primary.Sound			= Sound("Weapon_Glock.Single")
SWEP.Primary.BullettimeSound		= Sound("weapons/glock/glock18-1.wav")
SWEP.Primary.BullettimeSoundPitch	= 70
SWEP.Primary.Damage			= 11
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.023
SWEP.Primary.ConeZoomed		= 0.009
SWEP.Primary.Delay			= 0.12

SWEP.Primary.ClipSize		= 18
SWEP.Primary.DefaultClip	= 187
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "pistol"

SWEP.Recoil					= 0.76

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

if CLIENT then killicon.AddFont("weapon_glock", "CSKillIcons", "c", Color(255, 220, 0, 255)) end
