SWEP.Base					= "weapon_base"
SWEP.Slot 					= 1
SWEP.SlotPos				= 0
SWEP.PrintName				= "Knife"
SWEP.DrawCrosshair			= false
SWEP.Holstered				= false
SWEP.HoldType				= SWEP.Holstered and "normal" or "fist"

SWEP.ViewModelFOV			= 85
SWEP.ViewModel				= "models/weapons/v_knife_t.mdl"
SWEP.WorldModel				= "models/weapons/w_knife_t.mdl"

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.AutoSwitchTo			= false
SWEP.AutoSwitchFrom			= true
SWEP.Weight					= 1

SWEP.MissSound 				= Sound("weapons/knife/knife_slash1.wav")
SWEP.WallSound 				= Sound("weapons/knife/knife_hitwall1.wav")
SWEP.DeploySound			= Sound("weapons/knife/knife_deploy1.wav")
SWEP.Ragdollhit 			= Sound("weapons/knife/knife_stab.wav")

function SWEP:Deploy()
	self.Holstering = false
	self:SetHoldType(self.HoldType)
	if not self.Holstered then
		self:SendWeaponAnim(ACT_VM_DRAW)
		self:EmitSound(self.DeploySound, 50, 100)
	end
	self:SetNextPrimaryFire(CurTime() + 1)
	self:SetNextSecondaryFire(CurTime() + 1)

	return true
end

function SWEP:SecondaryAttack()
	if self.Holstered then return end
	local tr = {}
	tr.start = self.Owner:GetShootPos()
	tr.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 50)
	tr.filter = self.Owner
	tr.mask = MASK_SHOT

	local trace = util.TraceLine(tr)
	self.Weapon:SetNextPrimaryFire(CurTime() + 1)
	self.Weapon:SetNextSecondaryFire(CurTime() + 1.1)
	self.Owner:SetAnimation(PLAYER_ATTACK1)

	if trace.Hit then
		if trace.Entity:IsPlayer() then
			self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)

			bullet = {}
			bullet.Num    = 1
			bullet.Src    = self.Owner:GetShootPos()
			bullet.Dir    = self.Owner:GetAimVector()
			bullet.Spread = Vector(0, 0, 0)
			bullet.Tracer = 0
			bullet.Force  = 10
			bullet.Damage = 60

			self.Owner:FireBullets(bullet)
			self.Weapon:EmitSound(self.Ragdollhit, 100, math.random(80, 100))
		else
			self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)

			bullet = {}
			bullet.Num    = 1
			bullet.Src    = self.Owner:GetShootPos()
			bullet.Dir    = self.Owner:GetAimVector()
			bullet.Spread = Vector(0, 0, 0)
			bullet.Tracer = 0
			bullet.Force  = 10
			bullet.Damage = 60

			self.Owner:FireBullets(bullet)
			self.Weapon:EmitSound(self.WallSound, 100, math.random(80, 100))
		end
	else
		self.Weapon:EmitSound(self.MissSound, 100, math.random(80, 100))
		self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
		self.Weapon:SetNextPrimaryFire(CurTime() + 1)
	end
end

function SWEP:PrimaryAttack()
	if self.Holstered then return end
	local tr = {}
	tr.start = self.Owner:GetShootPos()
	tr.endpos = self.Owner:GetShootPos() + (self.Owner:GetAimVector() * 50)
	tr.filter = self.Owner
	tr.mask = MASK_SHOT
	local trace = util.TraceLine(tr)

	self.Weapon:SetNextSecondaryFire(CurTime() + 0.5)
	self.Weapon:SetNextPrimaryFire(CurTime() + 0.5)
	self.Owner:SetAnimation(PLAYER_ATTACK1)

	if trace.Hit then
		if trace.Entity:IsPlayer() then
			self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)

			bullet = {}
			bullet.Num    = 1
			bullet.Src    = self.Owner:GetShootPos()
			bullet.Dir    = self.Owner:GetAimVector()
			bullet.Spread = Vector(0, 0, 0)
			bullet.Tracer = 0
			bullet.Force  = 5
			bullet.Damage = 20

			self.Owner:FireBullets(bullet)
			self.Weapon:EmitSound("weapons/knife/knife_hit" .. math.random(1, 4) .. ".wav")
		else
			self.Weapon:SendWeaponAnim(ACT_VM_HITCENTER)

			bullet = {}
			bullet.Num    = 1
			bullet.Src    = self.Owner:GetShootPos()
			bullet.Dir    = self.Owner:GetAimVector()
			bullet.Spread = Vector(0, 0, 0)
			bullet.Tracer = 0
			bullet.Force  = 5
			bullet.Damage = 20

			self.Owner:FireBullets(bullet)
			self.Weapon:EmitSound(self.WallSound, 100, math.random(80, 100))
		end
	else
		self.Weapon:EmitSound(self.MissSound, 100, math.random(80, 100))
		self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
		self.Weapon:SetNextSecondaryFire(CurTime() + 1)
	end
end

SWEP.NextReload = CurTime()
function SWEP:Reload()
	if self.NextReload <= CurTime() and self:GetNextPrimaryFire() <= CurTime() and self:GetNextSecondaryFire() <= CurTime() then
		self.Holstered = not self.Holstered
		self:SetHoldType(self.Holstered and "normal" or "fist")
		self.NextReload = CurTime() + 1
	end
end

local toPos = SWEP.Holstered and 1 or 0
if CLIENT then
	function SWEP:Holster()
		self.Holstering = true
	end

	function SWEP:CalcViewModelView(vm, oldPos, oldAng, pos, ang)
		local vec = Vector(-1, 0, -4)
		vec:Rotate(ang)
		if self.Holstered then
			toPos = Lerp(FrameTime() * 1, toPos, 1)
		else
			toPos = Lerp(FrameTime() * 1, toPos, 0)
		end
		local boneShit = self.Holstering and 0 or toPos
		vm:ManipulateBoneAngles(vm:LookupBone("v_weapon.Right_Hand"), Angle(0, 0, 0	) * boneShit)
		vm:ManipulateBoneAngles(vm:LookupBone("v_weapon.Right_Arm"), Angle(-20, 5, -100) * boneShit)
		vm:ManipulateBoneAngles(vm:LookupBone("v_weapon.Knife_Handle"), Angle(80, -30, 0) * boneShit)
		vm:ManipulateBonePosition(vm:LookupBone("v_weapon.Knife_Handle"), Vector(-5.5, -2, -1) * boneShit)
		return pos + vec * toPos, ang
	end
end

function SWEP:CustomAmmoDisplay() return {} end

SWEP.IconLetter = "j"

function SWEP:DrawWeaponSelection(x, y, wide, tall, alpha)
	draw.SimpleText(self.IconLetter, self.IconLetterFont or "CSSelectIcons", x + wide/2, y + tall*0.22, Color(255, 220, 0, alpha), TEXT_ALIGN_CENTER)
	draw.SimpleText("Holster by pressing R", "hudselectiontext", x + wide/2, y + tall*0.60, Color(255, 220, 0, alpha), TEXT_ALIGN_CENTER)
end
