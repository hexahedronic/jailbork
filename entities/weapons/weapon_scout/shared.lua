AddCSLuaFile()

SWEP.Base = "weapon_twitch_base"

SWEP.HoldType			= "smg"
SWEP.PrintName				= "Scout"

SWEP.Slot					= 4
SWEP.IconLetter				= "n"

SWEP.ViewModel				= "models/weapons/v_snip_scout.mdl"
SWEP.WorldModel				= "models/weapons/w_snip_scout.mdl"
SWEP.ViewModelAimPos		= Vector(4.9601, -0.8142, 0.206)
SWEP.ViewModelAimAng		= Vector(2.9663, 1.616, 3.3092)
SWEP.ViewModelFlip			= true

SWEP.DrawAmmo				= false
SWEP.DrawCrosshair			= false

SWEP.Primary.Sound			= Sound("Weapon_Scout.Single")
SWEP.Primary.Damage			= 45
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.004
SWEP.Primary.ConeZoomed		= 0.0007
SWEP.Primary.Delay			= 1.2

SWEP.Primary.ClipSize		= 10
SWEP.Primary.DefaultClip	= 40
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "SniperRound"

SWEP.Recoil					= 2.8

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

if CLIENT then killicon.AddFont("weapon_scout", "CSKillIcons", "n", Color(255, 220, 0, 255)) end
