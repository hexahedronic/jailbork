AddCSLuaFile()
	
SWEP.HoldType			= "smg"

SWEP.Base = "weapon_twitch_base"

SWEP.PrintName				= "UMP45"

SWEP.Slot					= 2
SWEP.IconLetter				= "q"

SWEP.ViewModel				= "models/weapons/v_smg_ump45.mdl"
SWEP.WorldModel				= "models/weapons/w_smg_ump45.mdl"
--SWEP.ViewModelAimPos		= Vector(2.5364, -1.8409, 1.745)
SWEP.ViewModelAimPos		= Vector(6.3017, -1.4408, 2.0913)
SWEP.ViewModelAimAng		= Vector(-1.4458, 0.2337, 2.5401)
SWEP.ViewModelFlip			= true

SWEP.DrawAmmo				= false
SWEP.DrawCrosshair			= false

SWEP.Primary.Sound			= Sound("Weapon_UMP45.Single")
SWEP.Primary.BullettimeSound		= Sound("weapons/ump45/ump45-1.wav")
SWEP.Primary.BullettimeSoundPitch	= 70
SWEP.Primary.Damage			= 18
SWEP.Primary.NumShots		= 1
SWEP.Primary.Cone			= 0.03
SWEP.Primary.ConeZoomed		= 0.006
SWEP.Primary.Delay			= 0.1

SWEP.Primary.ClipSize		= 26
SWEP.Primary.DefaultClip	= 250
SWEP.Primary.Automatic		= true
SWEP.Primary.Ammo			= "smg1"

SWEP.Recoil					= 0.5

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

if CLIENT then killicon.AddFont("weapon_ump45", "CSKillIcons", "q", Color(255, 220, 0, 255)) end
