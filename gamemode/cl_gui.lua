local background = Color(000, 000, 000, 192)
local disabled = Color(155, 155, 155, 255)

function GM:CreateGenericButton(par, w, h)
	local par_w, par_h = par:GetSize()
	
	local pan = vgui.Create("DButton", par)
	pan:SetSize(w, h)
	
	pan:SetText("")
	pan.SetText = function(p, t)
		p.custom_title = t
	end
	
	pan.Paint = function(p, w, h)
		surface.SetDrawColor(background)
		surface.DrawRect(0, 0, w, h)
		
		draw.SimpleText(p.custom_title or "", "BudgetLabel", w / 2, h / 2, p.m_bDisabled and disabled or color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end
	
	return pan
end

function GM:CreateGenericGUI(ws, hs)
	local pan = vgui.Create("DFrame")
	pan:SetSize(ScrW() * ws, ScrH() * hs)
	
	pan:SetTitle("")
	pan.SetTitle = function(p, t)
		p.custom_title = t
	end
	
	local head = 22
	
	pan:ShowCloseButton(false)
	pan.close_button = self:CreateGenericButton(pan, 58, 22)
	pan.close_button:SetText("Close")
	pan.close_button.DoClick = function(p) pan:Close() end
	
	pan.layout = function(p, w, h) end
	pan.PerformLayout = function(p, w, h)
		local pw, ph = p.close_button:GetSize()
		p.close_button:SetPos(w - pw - 1, 0)
		
		p.layout(p, w, h)
	end
	
	pan.Paint = function(p, w, h)
		surface.SetDrawColor(background)
		draw.NoTexture()
		
		surface.DrawRect(0, 0, w, h)
		surface.DrawRect(0, 0, w, head)
		
		draw.SimpleText(p.custom_title or "", "BudgetLabel", w / 2, head / 2, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)
	end

	pan:Center()
	pan:MakePopup()
	
	return pan
end

function GM:ToggleSwitcherGUI()
	if SWITCH_GUI and IsValid(SWITCH_GUI) then SWITCH_GUI:Close() return end
	SWITCH_GUI = GAMEMODE:CreateGenericGUI(0.3, 0.15)
	SWITCH_GUI:SetTitle("Jailbreak Switcher")

	local wa, ha = SWITCH_GUI:GetSize()
	SWITCH_GUI.toggle = GAMEMODE:CreateGenericButton(SWITCH_GUI, wa - 10, ((ha - 22) * 1 / 3) - 7.5)
	SWITCH_GUI.toggle:SetText("Toggle Team Switch")
	SWITCH_GUI.toggle.DoClick = function(p) RunConsoleCommand"jb_queue_switch" SWITCH_GUI:Close() end

	SWITCH_GUI.raffle = GAMEMODE:CreateGenericButton(SWITCH_GUI, wa - 10, ((ha - 22) * 1 / 3) - 7.5)
	SWITCH_GUI.raffle:SetText("Toggle Warden Raffle (Guard)")
	SWITCH_GUI.raffle:SetEnabled(LocalPlayer():Team() == TEAM_GUARD)
	SWITCH_GUI.raffle.DoClick = function(p) RunConsoleCommand"jb_queue_warden" SWITCH_GUI:Close() end
	
	SWITCH_GUI.pvp = GAMEMODE:CreateGenericButton(SWITCH_GUI, wa - 10, ((ha - 22) * 1 / 3) - 7.5)
	SWITCH_GUI.pvp:SetText("Toggle PVP (Warden)")
	SWITCH_GUI.pvp:SetEnabled(GAMEMODE.Warden and Entity(GAMEMODE.Warden) == LocalPlayer())
	SWITCH_GUI.pvp.DoClick = function(p) RunConsoleCommand"jb_toggle_td" SWITCH_GUI:Close() end

	SWITCH_GUI.layout = function(p, w, h)
		local pw, ph = p.toggle:GetSize()
		p.toggle:SetPos(w / 2 - pw / 2, 22 + 5)
		
		local pw2, ph2 = p.raffle:GetSize()
		p.raffle:SetPos(w / 2 - pw2 / 2, 22 + 10 + ph)
		
		local pw3, ph3 = p.pvp:GetSize()
		p.pvp:SetPos(w / 2 - pw2 / 2, 22 + 15 + ph + ph2)
	end
end
