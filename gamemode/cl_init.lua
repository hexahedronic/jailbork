include("sh_init.lua")
include("sh_round.lua")

include("chat/chatbox.lua")
include("chat/chathud_markup.lua")
include("chat/chathud.lua")

include("cl_gui.lua")

language.Add("SniperRound_Ammo", "Sniper Rounds")

GM.Warden = 0

local hud_shoulddraw
do
	local hud_dontdraw = {
		--LS_Core_HUDPaint = true,
		CHudHealth = true,
		CHudBattery = true,
	}
	function hud_shoulddraw(e)
		if hud_dontdraw[e] then return false end
	end
end
hook.Add("HUDShouldDraw", "hud_shoulddraw", hud_shoulddraw)

local f = "Franklin Gothic Medium"
surface.CreateFont("NormalFont", {
	font = f,
	size = 20,
	weight = 1,
	outline = false,
})

surface.CreateFont("RoundFont", {
	font = f,
	size = 22,
	weight = 750,
	outline = false,
})

local hud_main
do
	if hud_avatar_panel then hud_avatar_panel:Remove() end
	hud_avatar_panel = vgui.Create("AvatarImage")
	
	local panel_init = false
	local background = Color(000, 000, 000, 192)
	
	local slope = 10
	
	local user = Material("icon16/user.png")
	local heart = Material("icon16/heart.png")
	local shield = Material("icon16/shield.png")
	local world = Material("icon16/world.png")
	local clock = Material("icon16/clock.png")
	local lightning = Material("icon16/lightning.png")
	local function surface_draw_bar(x, y, w, h, col, ico)
		surface.SetDrawColor(background)
		draw.NoTexture()
		
		local struct = {
			{x = x + w, y = y - h},
			{x = x + w - slope, y = y},
			{x = x, y = y},
			{x = x, y = y - h},
		}
		surface.DrawPoly(struct)
		surface.DrawRect(x, y - h, h, h)
		
		surface.SetMaterial(ico or user)
		surface.SetDrawColor(255, 255, 255, 255)
		
		surface.DrawTexturedRect(x + h/2 - 8, y - h + h/2 - 8, 16, 16)
	end
	
	local function surface_draw_ico(x, y, s, i)
		surface.SetMaterial(i or user)
		surface.SetDrawColor(255, 255, 255, 255)
		
		surface.DrawTexturedRect(x, y, s, s)
	end
	
	local update = ScrW()
	local last_p = NULL
	function hud_main()
		local spectate = Entity(GAMEMODE.Spectating or 0)
		local s_w, s_h = ScrW(), ScrH()
		local c, i, s = 0, 20, 3 -- Current, Interval, Spacing
		local ow, oh = 10, 10
		local p = IsValid(spectate) and spectate or LocalPlayer()
		
		local w = 220 + (64 + 3) + 1
		
		local function add_c(n) c = c + n + s end
		local function add_w(n) w = w + slope + s end
 		local function h() return s_h - oh - c end
		
		-- Dont forget this is bottom -> top
			
		if not panel_init or s_w ~= update or p ~= last_p then
			hud_avatar_panel:SetPlayer(p, 64)
			hud_avatar_panel:SetPos(ow, h() - 64 - 1)
			hud_avatar_panel:SetSize(64, 64)
			hud_avatar_panel.Think = function() local a = hook.Run("HUDShouldDraw", "hud_main") if not a or gui.IsGameUIVisible() then hud_avatar_panel:SetVisible(false) end end
			
			panel_init = true
			update = s_w
		end
		
		last_p = p
		hud_avatar_panel:SetVisible(true)
		
		ow = ow +(64 + 3) + 1
		w = w -(64 + 3) - 1
		
		surface_draw_bar(ow, h(), w, i, background, shield)
			draw.SimpleText(p:Armor() .. " / 255", "NormalFont", ow + i + s, h() - i/2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			add_c(i) add_w(slope)
		surface_draw_bar(ow, h(), w, i, background, heart)
			draw.SimpleText(p:Health() .. " / " .. p:GetMaxHealth(), "NormalFont", ow + i + s, h() - i/2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			add_c(i) add_w(slope)
		--[[surface_draw_bar(ow, h(), w, i, background, user)
			draw.SimpleText(p:Nick(), "BudgetLabel", ow + i + s, h() - i/2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			add_c(i) add_w(slope)]]
			
		local ward = Entity(GAMEMODE.Warden)
		
		surface_draw_bar(ow, h(), w, i, background, world)
			draw.SimpleText(team.GetName(p:Team()) .. " | PvP: " .. (GAMEMODE.TeamDamageEnabled and "On" or "Off"), "NormalFont", ow + i + s, h() - i/2, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
			add_c(i) add_w(slope)
			
		ow = ow -(64 + 3) - 1
		w = w +(64 + 3) + 1
			
		local statustext = ""
		local time = GAMEMODE:GetTimeLeft()
		
		if GAMEMODE:GetRoundState() == ROUND_ONGOING then
			statustext = statustext .. string.FormattedTime(time, "%02i:%02i")
		elseif GAMEMODE:GetRoundState() == ROUND_SETUP then
			statustext = statustext .. string.FormattedTime(GAMEMODE.Config.RoundTime, "%02i:%02i") .. " + " .. string.FormattedTime(math.max(time, 0), "%02i:%02i")
			if p:Team() == TEAM_PRISONER then statustext = statustext .. " (MUTED)" end
		else
			statustext = statustext .. "Round Over!"
		end
		
		s = 6
		
		surface_draw_ico(10, 10, 24, clock)
		draw.SimpleTextOutlined(statustext, "RoundFont", 44, 10 + 12, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, 1, color_black)
		
		local wardentext = GAMEMODE:GetRoundState() == ROUND_SETUP and "No Warden" or "Warden disconnected"
		if IsValid(ward) then
			if ward == LocalPlayer() and p:Alive() then
				wardentext = "You are the Warden"
			elseif ward:Alive() then
				wardentext = ward:Nick()
			else
				wardentext = "Warden is dead"
			end
		end
		
		surface_draw_ico(10, 10 + 24 + s, 24, lightning)
		draw.SimpleTextOutlined(wardentext, "RoundFont", 44, 10 + 24 + s + 12, color_white, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, 1, color_black)
		
		if IsValid(spectate) then
			draw.SimpleTextOutlined(spectate:Nick(), "RoundFont", ScrW() / 2, ScrH() - 10, team.GetColor(p:Team()), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, color_black)
			draw.SimpleTextOutlined("YOU ARE CURRENTLY SPECTATING", "NormalFont", ScrW() / 2, ScrH() - 32, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, color_black)
		end
	end
end
hook.Add("HUDPaint", "hud_main", hud_main)

function GM:PlayerBindPress(ply, bind, pressed)
	if bind == "+menu" and pressed then
		RunConsoleCommand("jb_drop_weapon")
		
		return true
	elseif bind == "gm_showspare2" and pressed then
		self:ToggleSwitcherGUI()
		print"hi"
		
		return true
	elseif bind == "gm_showhelp" and pressed then
		RunConsoleCommand("ctp")
		
		return true
	end
end
