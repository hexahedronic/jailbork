include("sh_init.lua")

include("maps.lua")

AddCSLuaFile("chat/chatbox.lua")
AddCSLuaFile("chat/chathud_markup.lua")
AddCSLuaFile("chat/chathud.lua")

AddCSLuaFile("cl_gui.lua")

GM.Guard_Queue = {}
GM.Prisoner_Queue = {}
GM.Warden_Raffle = {}

GM.Warden = NULL

function GM:PlayerCanPickupWeapon(p, e)
	local ent = p:GetEyeTrace() and p:GetEyeTrace().Entity
	local use = p:KeyDown(IN_USE) or false
	local time = p.Wep_Time or 0
	
	if self.Config.WeaponIgnore[e:GetClass()] or e.Given then
		p.Wep_InUse = nil
		
		-- Weapon is hands
		return true
	elseif use then
		if ent ~= e or p.Wep_InUse or time > CurTime() then
			return false
		else
			p.Wep_InUse = true
			p.Wep_Time = CurTime() + GAMEMODE.Config.WeaponPickupTime
			
			return true
		end
	else
		p.Wep_InUse = nil
		
		return false
	end
end

function GM:PlayerSpawn(p)
	self.BaseClass:PlayerSpawn(p)
	
	if p:Team() == TEAM_GUARD then
		p:SetArmor(100)
		p:AllowFlashlight(true)
	else
		p:AllowFlashlight(false)
	end
	
	local mdl = GAMEMODE.Config.Models[p:Team()]
	if mdl and table.Count(mdl) > 0 then
		p:SetModel(table.Random(mdl))
	end
	
	p:UnSpectate()
	p:SetNoCollideWithTeammates(true)
	
	p.SpecTarget = nil
	p.SpecEnt = nil
	
	p:SetJumpPower(190)

	net.Start("JB_Spectator")
		net.WriteUInt(0, 16)
	net.Send(p)
end

function GM:PlayerInitialSpawn(p)
	self.BaseClass:PlayerInitialSpawn(p)
	
	timer.Simple(0, function() if IsValid(p) then hook.Run("PostInitialSpawn", p) end end)
end

function GM:AutoBalance(pl)
	local per = self.Config.GuardPercent
	local p = team.GetPlayers(TEAM_PRISONER)
	local pris = #p
	local g = team.GetPlayers(TEAM_GUARD)
	local guard = #g
	local need = math.ceil(self.Config.GuardPercent * pris)
	
	if pl then
		if guard < need and #self.Guard_Queue == 0 then pl:SetTeam(TEAM_GUARD) end
		return
	end
	
	while math.min(#self.Prisoner_Queue, #self.Guard_Queue) ~= 0 do -- Perform 2 way interchange if both queues have people
		local a = self.Guard_Queue[1]
		local b = self.Prisoner_Queue[1]
		
		self:ClearPlayerFromQueues(a)
		self:ClearPlayerFromQueues(b)
		
		a:SetTeam(TEAM_GUARD)
		b:SetTeam(TEAM_PRISONER)
	end
	
	p = team.GetPlayers(TEAM_PRISONER)
	g = team.GetPlayers(TEAM_GUARD)
	
	if need == guard or pris < 2 then return end
	
	while guard > need + 1 do -- Get rid of un-needed guards
		local a = #self.Prisoner_Queue ~= 0 and self.Prisoner_Queue[1] or table.Random(g)
		a:SetTeam(TEAM_PRISONER)
		self:ClearPlayerFromQueues(a)
		
		p = team.GetPlayers(TEAM_PRISONER)
		pris = #p
		g = team.GetPlayers(TEAM_GUARD)
		guard = #g
		need = math.ceil(self.Config.GuardPercent * pris)
	end
	
	while guard < need do -- Get new guards
		local a = #self.Guard_Queue ~= 0 and self.Guard_Queue[1] or table.Random(p)
		a:SetTeam(TEAM_GUARD)
		self:ClearPlayerFromQueues(a)
		
		p = team.GetPlayers(TEAM_PRISONER)
		pris = #p
		g = team.GetPlayers(TEAM_GUARD)
		guard = #g
		need = math.ceil(self.Config.GuardPercent * pris)
	end
end

function GM:PostInitialSpawn(p)
	p:SetTeam(TEAM_PRISONER)
	self:AutoBalance(p)
	p:KillSilent()
	
	if self:GetRoundState() == ROUND_SETUP then
		p:Spawn()
	else
		p:Spectate(OBS_MODE_ROAMING)
	end
end

function GM:PlayerSelectSpawn(p)
	local spawns = team.GetSpawnPoints(p:Team())
	if not spawns or #spawns == 0 then return self.BaseClass:PlayerSelectSpawn(p) end
	
	return table.Random(spawns)
end

function GM:PlayerCanHearPlayersVoice(a, b)
	if not b:Alive() and self:GetRoundState() ~= ROUND_ENDED then
		if not a:Alive() then
			return true
		end
		
		return false
	end
	
	if b:Team() == TEAM_PRISONER and self:GetRoundState() == ROUND_SETUP then
		return false
	end
	
	return true
end

function GM:PlayerCanSeePlayersChat(t, team, a, b)
	if not IsValid(a) or not IsValid(b) then return true end
	
	if not b:Alive() and self:GetRoundState() ~= ROUND_ENDED then
		if not a:Alive() then
			return true
		end
		
		return false
	end
	
	return true
end

function GM:PlayerDeath(ply, inflictor, attacker)
	ply.NextSpawnTime = CurTime()
	ply.DeathTime = CurTime()
	
	ply:ThrowWeapon()
end

function GM:PlayerShouldTakeDamage(a, b)
	if a:IsPlayer() and b:IsPlayer() and a:Team() == b:Team() and self.TeamDamageEnabled ~= true then
		return false
	end
	
	return true
end

concommand.Add("jb_drop_weapon", function(p)
	if IsValid(p) then
		local wep = p:GetActiveWeapon()
		
		if IsValid(wep) and wep:GetClass() ~= "hands" and wep:GetClass() ~= "weapon_knife" then
			p:ThrowWeapon()
		end
	end
end)

function GM:PostPlayerDeath(p)
	p:Spectate(OBS_MODE_ROAMING)
end

function GM:RemoveNULLFromQueue()
	for k, v in pairs(self.Guard_Queue) do
		if not IsValid(v) then table.remove(self.Guard_Queue, k) end
	end
	for k, v in pairs(self.Prisoner_Queue) do
		if not IsValid(v) then table.remove(self.Prisoner_Queue, k) end
	end
	for k, v in pairs(self.Warden_Raffle) do
		if not IsValid(v) then table.remove(self.Warden_Raffle, k) end
	end
end

function GM:ClearPlayerFromQueues(p, igw)
	for k, v in pairs(self.Guard_Queue) do
		if v == p then table.remove(self.Guard_Queue, k) end
	end
	for k, v in pairs(self.Prisoner_Queue) do
		if v == p then table.remove(self.Prisoner_Queue, k) end
	end
	p.InQueue = nil
	
	if igw then return end
	
	for k, v in pairs(self.Warden_Raffle) do
		if v == p then table.remove(self.Warden_Raffle, k) end
	end
	p.WantsWarden = nil
end

function GM:PlayerDisconnect(p)
	self:ClearPlayerFromQueues(p)
end

concommand.Add("jb_queue_switch", function(p)
	if IsValid(p) and (not p.NextCmd or p.NextCmd < CurTime()) then
		if not p.InQueue then
			p.InQueue = true
			
			if p:Team() == TEAM_GUARD then
				table.insert(GAMEMODE.Prisoner_Queue, p)
			else
				table.insert(GAMEMODE.Guard_Queue, p)
			end
		else
			p.InQueue = false
			
			GAMEMODE:ClearPlayerFromQueues(p, true)
		end
		
		p:ChatPrint("You " .. (p.InQueue and "joined" or "left") .. " the switch queue.")
		MsgN(p:Nick() .. " -> Switch Queue " .. (p.InQueue and "On" or "Off"))
		p.NextCmd = CurTime() + 1
		
		GAMEMODE.RoundUpdate()
	end
end)

concommand.Add("jb_queue_warden", function(p)
	if IsValid(p) and p:Team() == TEAM_GUARD and (not p.NextCmd or p.NextCmd < CurTime()) then
		if not p.WantsWarden then
			p.WantsWarden = true
			
			table.insert(GAMEMODE.Warden_Raffle, p)
		else
			p.WantsWarden = false
			
			for k, v in pairs(GAMEMODE.Warden_Raffle) do
				if v == p then table.remove(GAMEMODE.Warden_Raffle, k) end
			end
		end
		
		p:ChatPrint("You " .. (p.WantsWarden and "joined" or "left") .. " the warden pool.")
		MsgN(p:Nick() .. " -> Warden Raffle " .. (p.WantsWarden and "On" or "Off"))
		p.NextCmd = CurTime() + 1
		
		GAMEMODE.RoundUpdate()
	end
end)

concommand.Add("jb_toggle_td", function(p)
	if IsValid(p) and p == GAMEMODE.Warden and p:Alive() then
		GAMEMODE.TeamDamageEnabled = not GAMEMODE.TeamDamageEnabled
		
		p:ChatPrint("Team Damage: " .. (GAMEMODE.TeamDamageEnabled and "ENABLED" or "DISABLED"))
		MsgN(p:Nick() .. " -> Team Damage " .. (GAMEMODE.TeamDamageEnabled and "On" or "Off"))
		
		GAMEMODE.RoundUpdate()
	end
end)
