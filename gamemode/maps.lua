hook.Add("OnRoundStateChange", "JB_OpenDoors", function(state)
	if state ~= ROUND_ONGOING then return end
	local map = game.GetMap()
	
	if map == "ba_jail_blackops" then
		local but = ents.GetMapCreatedEntity(1754)
		local door = ents.GetMapCreatedEntity(1510)
		
		if IsValid(but) and IsValid(door) then
			if door:GetSaveTable().m_toggle_state == 1 then but:Fire("use") end
		end
	elseif map == "jb_new_summer_v2" then
		local a = ents.GetMapCreatedEntity(2537)
		local b = ents.GetMapCreatedEntity(2211)
		
		if IsValid(a) and IsValid(b) then
			a:Remove()
			b:Remove()
		end
	elseif map == "ba_jail_cavewalk_v1b" then
		local but = ents.GetMapCreatedEntity(1680)
		if IsValid(but) then
			if but:GetSaveTable().m_flMoveDistance < 1 then but:Fire("use") end
		end
	elseif map == "ba_jail_tile_paradice_v_2_sg" then
		local but = ents.GetMapCreatedEntity(1665)
		if IsValid(but) then
			but:Fire("use")
		end
	end
end)

local tvs = {
	jb_new_summer_v2 = {
		{
			Vector (1192.1723632813, 548.44482421875, 60.48653793335),
			-- 0x2c217400
			Angle (-0.01509677618742, 89.987411499023, 0.013728599064052),
		},
	},
	ba_jail_blackops = {
		{
			-- 0x252958e0
			Vector (-6388.166015625, 64.769401550293, -167.65649414063),
			-- 0x2698a888
			Angle (3.2805955925141e-006, -6.158986252558e-006, -2.1011048829678e-006),
		},
	},
	
	ba_jail_tile_paradice_v_2_sg = {
		{
			-- 0x24dbe4b8
			Vector (3105.4372558594, -306.73266601563, -1007.5979614258),
			-- 0x253823c8
			Angle (-0.053168643265963, 0.0059079322963953, -0.007171630859375),
		},
	},
}

hook.Add("OnRoundStateChange", "JB_MediaPlayer", function(state)
	if state ~= ROUND_SETUP then return end
	local map = game.GetMap()
	local tv = tvs[map]
	if not tv then return end
	
	for k, v in pairs(tv) do
		local e = ents.Create"mediaplayer_tv"
			e:SetPos(v[1])
			e:SetAngles(v[2])
			e:SetMoveType(MOVETYPE_NONE)
		e:Spawn()
		e:Activate()
	end
end)

function GM:RockTheVote()
	if not GVote then error"gvote not present" end
	local maps = file.Find("maps/ba_*.bsp", "GAME")
	local maps2 = file.Find("maps/jb_*.bsp", "GAME")
	for k, v in pairs(maps2) do
		table.insert(maps, v)
	end
	
	for k, v in pairs(maps) do
		maps[k] = v:gsub(".bsp", "")
		if maps[k] == game.GetMap() then table.remove(maps, k) end
	end
	
	if #maps == 0 then return end
	
	local m = {}
	for i = 1, math.min(8, #maps) do
		local t, a = table.Random(maps)
		table.remove(maps, a)
		table.insert(m, t)
	end
	
	m[#m+1] = function(b)
		local mostvoted = 0
		local votes = 0
		for k, v in ipairs(b) do if #v > votes then votes = #v mostvoted = k end end
		
		if mostvoted < 2 then for k, v in pairs(player.GetAll()) do v.RTV = nil end return end
		game.ConsoleCommand("changelevel " .. m[mostvoted - 1] .. "\n")
	end -- lua unpack hack
	GVote("Rock The Vote:", "Extend Current", unpack(m))
end

hook.Add("PlayerSay", "JB_RTV", function(p, t)
	if #t == 3 and t:sub(1, 3):lower() == "rtv" then
		p.RTV = not p.RTV
		
		local pr = player.GetAll()
		local r = 0
		for k, v in pairs(pr) do
			if v.RTV then r = r + 1 end
		end
		
		
		p:ChatPrint("You have " .. (not p.RTV and "Un-" or "") .. "Rocked The Vote! (" .. r .. "/" .. #pr .. ") " .. math.max(math.ceil(#pr/2), 2) .. " required")
	end
end)

hook.Add("OnRoundStateChange", "JB_RTV", function(state)
	local p = player.GetAll()
	if state ~= ROUND_ENDED or #p == 0 then return end
	
	local r = 0
	for k, v in pairs(p) do
		if v.RTV then r = r + 1 end
	end
	
	if r > math.max(1, #p / 2) then
		GAMEMODE:RockTheVote()
	end
end)
