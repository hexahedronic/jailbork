AddCSLuaFile()

GM.Name = "Jail Break"
GM.Author = "Q2F2, Ghosty"
GM.Contact = "notq2f2@gmail.com"
GM.Website = "hexahedron.pw"

GM.TeamDamageEnabled = false

include("chat/chatexp.lua")

include("sh_util.lua")
include("sh_round.lua")

TEAM_PRISONER = 1
team.SetUp(TEAM_PRISONER, "Prisoners", Color(200, 80, 80))
team.SetSpawnPoint(TEAM_PRISONER, {"info_player_terrorist"})

TEAM_GUARD = 2
team.SetUp(TEAM_GUARD, "Guards", Color(80, 80, 200))
team.SetSpawnPoint(TEAM_GUARD, {"info_player_counterterrorist"})

GM.Config = {}
GM.Config.Models = {}

GM.Config.WeaponPickupTime = 1.2
GM.Config.SetupTime = 40
GM.Config.RoundTime = 9*60
GM.Config.EndTime = 10

GM.Config.GuardPercent = 0.2

GM.Config.WeaponIgnore = {}
GM.Config.WeaponIgnore.hands = true
GM.Config.WeaponIgnore.weapon_knife = true
GM.Config.WeaponIgnore.weapon_physgun = true

GM.Config.Models[TEAM_PRISONER] = {
	"models/player/group01/male_01.mdl",
	"models/player/group01/male_02.mdl",
	"models/player/group01/male_03.mdl",
	"models/player/group01/male_04.mdl",
	"models/player/group01/male_05.mdl",
	"models/player/group01/male_06.mdl",
	"models/player/group01/male_07.mdl",
	"models/player/group01/male_08.mdl",
	"models/player/group01/male_09.mdl",
}
GM.Config.Models[TEAM_GUARD] = {
	"models/player/swat.mdl",
}
GM.Config.WardenModel = "models/player/riot.mdl"
