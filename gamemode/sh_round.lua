AddCSLuaFile()

function GM:CanStartRound()
	local p = player.GetAll()
	local s = team.NumPlayers(TEAM_SPECTATOR)
	return (#p - s) > 1
end

ROUND_SETUP = 0
ROUND_ONGOING = 1
ROUND_ENDED = 2

GM.EndTime = GM.EndTime or (CurTime() + 20)
GM.RoundState = GM.RoundState or ROUND_ENDED

function GM:GetTimeLeft()
	return self.EndTime - CurTime()
end

function GM:GetRoundState()
	return self.RoundState
end

if CLIENT then
	net.Receive("JB_RoundInfo", function(len)
		GAMEMODE.EndTime = net.ReadUInt(32)
		GAMEMODE.RoundState = net.ReadUInt(8)
		
		GAMEMODE.Warden = net.ReadUInt(16)
		
		GAMEMODE.TeamDamageEnabled = net.ReadBool()
	end)

	net.Receive("JB_Spectator", function(len)
		GAMEMODE.Spectating = net.ReadUInt(16)
	end)
else
	util.AddNetworkString("JB_RoundInfo")
	
	function GM.RoundUpdate(p)
		net.Start("JB_RoundInfo")
			net.WriteUInt(GAMEMODE.EndTime, 32)
			net.WriteUInt(GAMEMODE.RoundState, 8)
			
			net.WriteUInt(IsValid(GAMEMODE.Warden) and GAMEMODE.Warden:EntIndex() or 0, 16)
			
			net.WriteBool(GAMEMODE.TeamDamageEnabled)
		if p then net.Send(p) else net.Broadcast() end
	end
	
	hook.Add("PostInitialSpawn", "JB_RoundInfo", GM.RoundUpdate)
	hook.Add("OnReloaded", "JB_RoundInfo", GM.RoundUpdate)
	
	local last_dont
	function GM:Think()
		self.BaseClass:Think()
		
		if (#team.GetLivePlayers(TEAM_GUARD) == 0 or #team.GetLivePlayers(TEAM_PRISONER) == 0) and self:CanStartRound() and self:GetRoundState() ~= ROUND_ENDED then
			self.RoundState = ROUND_ENDED
			self.EndTime = CurTime() + self.Config.EndTime
			
			hook.Run("OnRoundStateChange", ROUND_ENDED)
			self.RoundUpdate()
			
			return
		end
		
		local dont = not self:CanStartRound()
		
		if self:GetTimeLeft() < 1 then
			self:RemoveNULLFromQueue()
			
			if self:GetRoundState() == ROUND_ONGOING then
				self.RoundState = ROUND_ENDED
				self.EndTime = CurTime() + self.Config.EndTime
				
				hook.Run("OnRoundStateChange", ROUND_ENDED)
				self.RoundUpdate()
			elseif self:GetRoundState() == ROUND_ENDED and not (dont or last_dont) then
				self.RoundState = ROUND_SETUP
				self.EndTime = CurTime() + self.Config.SetupTime
				
				game.CleanUpMap()
				self:AutoBalance()
				
				GAMEMODE.TeamDamageEnabled = false
				
				for k, v in pairs(player.GetAll()) do
					if v:Team() ~= TEAM_SPECTATOR then
						v:KillSilent()
						v:Spawn()
					end
				end
				
				self.Warden = table.Random(table.Count(self.Warden_Raffle) ~= 0 and self.Warden_Raffle or team.GetPlayers(TEAM_GUARD))
				
				if IsValid(self.Warden) then
					self.Warden:SetArmor(255)
					self.Warden:SetModel(self.Config.WardenModel)
				end
				
				hook.Run("OnRoundStateChange", ROUND_SETUP)
				self.RoundUpdate()
			elseif self:GetRoundState() == ROUND_SETUP then
				self.RoundState = ROUND_ONGOING
				self.EndTime = CurTime() + self.Config.RoundTime
				
				if not (self.Warden and IsValid(self.Warden)) then
					self.Warden = table.Random(table.Count(self.Warden_Raffle) ~= 0 and self.Warden_Raffle or team.GetPlayers(TEAM_GUARD))
					
					if IsValid(self.Warden) then
						self.Warden:SetArmor(255)
						self.Warden:SetModel(self.Config.WardenModel)
					end
				end
				
				hook.Run("OnRoundStateChange", ROUND_ONGOING)
				self.RoundUpdate()
			end
		end
		
		last_dont = dont
	end
	
	util.AddNetworkString("JB_Spectator")
	
	function GM:DoSpectate(p, b)
		if p:Alive() then return end
		if b == IN_DUCK then
			if p.SpecTarget then
				p:Spectate(OBS_MODE_ROAMING)
				
				p.SpecTarget = nil
				p.SpecEnt = nil
			end
		else
			local t = player.GetLiving()
			if #t == 0 then return end
			
			p:Spectate(OBS_MODE_CHASE)
			local n = p.SpecTarget or 0
			
			if b == IN_ATTACK then n = n + 1
			else n = n - 1 end
		
			if n > #t then n = 1
			elseif n < 1 then n = #t end
			
			p.SpecTarget = n
			p.SpecEnt = t[n]
			p:SpectateEntity(p.SpecEnt)
		end
		
		net.Start("JB_Spectator")
			net.WriteUInt(IsValid(p.SpecEnt) and p.SpecEnt:EntIndex() or 0, 16)
		net.Send(p)
	end
	
	function GM:PlayerDeathThink(p)
		p.SpectatorKeys = p.SpectatorKeys or {}
		if p.SpecEnt and not (IsValid(p.SpecEnt) and p.SpecEnt:Alive()) then
			self:DoSpectate(p, IN_ATTACK)
			
			return false
		end
		
		if p:KeyDown(IN_ATTACK) and not p.SpectatorKeys[IN_ATTACK] then
			p.SpectatorKeys[IN_ATTACK] = true
			self:DoSpectate(p, IN_ATTACK)
		elseif not p:KeyDown(IN_ATTACK) then
			p.SpectatorKeys[IN_ATTACK] = nil
		end
		
		if p:KeyDown(IN_ATTACK2) and not p.SpectatorKeys[IN_ATTACK2] then
			p.SpectatorKeys[IN_ATTACK2] = true
			self:DoSpectate(p, IN_ATTACK2)
		elseif not p:KeyDown(IN_ATTACK2) then
			p.SpectatorKeys[IN_ATTACK2] = nil
		end
		
		if p:KeyDown(IN_DUCK) and not p.SpectatorKeys[IN_DUCK] then
			p.SpectatorKeys[IN_DUCK] = true
			self:DoSpectate(p, IN_DUCK)
		elseif not p:KeyDown(IN_DUCK) then
			p.SpectatorKeys[IN_DUCK] = nil
		end
		
		return false
	end
end
