AddCSLuaFile()

function team.GetLivePlayers(index)
	local TeamPlayers = {}

	for id, pl in next, player.GetAll() do
		if IsValid(pl) and pl:Team() == index and pl:Alive() then
			TeamPlayers[#TeamPlayers+1] = pl
		end
	end

	return TeamPlayers
end

function player.GetLiving()
	local TeamPlayers = {}
	
	for id, pl in next, player.GetAll() do
		if IsValid(pl) and pl:Alive() then
			TeamPlayers[#TeamPlayers+1] = pl
		end
	end

	return TeamPlayers
end
